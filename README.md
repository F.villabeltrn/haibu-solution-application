Primero ejecutar los comandos siguientes para preparar el ambiente:

    docker-compose up --build --remove-orphans

con eso, generaremos el contenedor donde se ejecutara el sistema. Aqui esta contenido lo siguiente:

    > Python 3.7.10
    > Flask 1.1.2

Una vez este todo realizado, simplemente ejecutamos el siguiente comando en la consola para iniciar el servidor:

    docker-compose up 

Para cerrar el servidor solo debe usar comando (crtl + c) en la consola donde ejecuto el servidor.

Posteriomente, para acceder a la pagina web es necesario el siguiente enlace en el navegador: 
    
    locahost:5000

Para ejecutar los Test Unitarios, ejecutar el siguiente comando en consola (Dentro de la misma carpeta donde se encuentra el main.py):

    docker-compose exec web python test_unitario.py
