lista_letras = ["A","B","C","D","E","F","G","H","I","J","K","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]

def aux_calcular(aux_uno,aux_dos):
    total = int(aux_uno) * 1000
    total += int(aux_dos) + 1
    return total

def calcular_id(patente,lista_letras=lista_letras):
    patente = patente.upper()
    contador = 0
    for a in lista_letras:
        for b in lista_letras:
            for c in lista_letras:
                for d in lista_letras:
                    #print(f'{a}{b}{c}{d}')
                    if patente[0] == a:
                        if patente[1] == b:
                            if patente[2] == c:
                                if patente[3] == d:
                                    return(aux_calcular(contador,patente[4:7]))
                    contador += 1

def calcular_patente(identificador, lista_letras=lista_letras):
    contador = 0
    for a in lista_letras:
        for b in lista_letras:
            for c in lista_letras:
                for d in lista_letras:
                    for e in range(1000):
                    #print(f'{a}{b}{c}{d}')
                        if contador == int(identificador):
                            e = e - 1
                            if 10 > e:
                                return(f'{a}{b}{c}{d}00{e}')
                            elif 100 > e:
                                return(f'{a}{b}{c}{d}0{e}')
                            return(f'{a}{b}{c}{d}{e}')
                        
                        contador += 1

def validar_patente(patente, lista_letras=lista_letras):
    
    letras = patente[0:4]
    numeros = patente[4:7]

    for a in letras:
        if a not in lista_letras:
            return False
    try:
        int(numeros)
    except ValueError:
        return False
    return True

def validar_id(identificador):
    try:
        identificador = int(identificador)
        if identificador > 0 and identificador <= 390625000:
            return(True)
        return False
    except ValueError:
        return False
    except:
        return False

def calcular_matriz(R,C,X,Y,Z):
    R = int(R)
    C = int(C)
    Z = int(Z)
    X = int(X)
    Y = int(Y)
    matriz_rc = [[Z + x - 1 for x in range(1,R+1)] for y in range(C)]

    matriz_xy = [[matriz_rc[a][a] for a in range(Y+1)] for b in range(X+1)]
    suma = 0
    for c in range(X+1):
        suma += sum(matriz_xy[c])

    return(suma)

def validar_numeros(R,C,X,Y,Z):
    validar_R ,validar_C ,validar_X ,validar_Y ,validar_Z = False, False, False, False, False
    try:
        #Compruebo si son enteros
        R = int(R)
        C = int(C)
        X = int(X)
        Y = int(Y)
        Z = int(Z)

        if R > 0:
            validar_R = True
        if C > 0:
            validar_C = True
        if X >= 0:
            validar_X = True
        if Y >= 0:
            validar_Y = True
        if 1000000 > Z > 0:
            validar_Z = True
        
        if Y >= R:
            return False
        
        if validar_R and validar_C and validar_X and validar_Y and validar_Z:

            return True
        return False
    except:
        return False
