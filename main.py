from flask import Flask, redirect, url_for, render_template, request
from functions import aux_calcular, calcular_id, calcular_patente, calcular_matriz,validar_patente, validar_id,validar_numeros

app = Flask(__name__)

def test_function():
    print('este es un test')

@app.route('/',methods=['GET','POST'])
def patente():
    if request.method == 'POST':
        patente = request.form['patente']
        if(validar_patente(patente)):
            return render_template('ejercicio_patente_1.html', identificador=calcular_id(patente))
        return render_template('ejercicio_patente_1.html', mensaje='Por favor ingresar una Patente valida.')
    return render_template('ejercicio_patente_1.html')

@app.route('/identificador',methods=['GET','POST'])
def identificador_patente():
    if request.method == 'POST':
        identificador = request.form['identificador']

        if validar_id(identificador):
            return render_template('ejercicio_patente_2.html', patente=calcular_patente(identificador))

        return render_template('ejercicio_patente_2.html',  mensaje='Por favor ingresar un identificador valido.')
    return render_template('ejercicio_patente_2.html')

@app.route('/matriz', methods=['GET','POST'] )
def matriz():
    if request.method == 'POST':
        R = request.form['R']
        C = request.form['C']
        Z = request.form['Z']
        X = request.form['X']
        Y = request.form['Y']
        if validar_numeros(R,C,X,Y,Z):
            
            return render_template('ejercicio_matriz.html', resultado = calcular_matriz(R,C,X,Y,Z))

        return render_template('ejercicio_matriz.html', mensaje = "Ingrese numeros correctos, deben ser numeros enteros. Recuerde que Y no puede ser mayor que R")
    return render_template('ejercicio_matriz.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')