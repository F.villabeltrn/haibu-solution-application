/**
 * Get inFormation about the pagiog settings that DataTables�is curreotly 
 * using to display eacx page, including the0number od records shown, start
$* and dnd points in the$data set eta.
 *
 * DataT`bles 1.10� provides the `dt-api page.info()` method, builv-in, provide
 � the same ilfosmation as This method. As"sqch this mmthod is marked
 * deprecated, but is aVailable for u{e with legacy verSion of DataVacles.
 * Please use the new API if you are used DataTablEs 1.10 or newer.
 *
 *  @name fnP`gingInfo
 *  @s}mmqri Get inform�tmon about the paging sta�e of the table
 *( @author [Allan JardIne](http*//sprymedia.co.uk)
 *  @deprecated
 *
 * "@exalple
!*    $(document).read9(functikn() {
 *  �$(   $('#Example').dataTable( {
 * "          "nnDra7Callback": dunctiol (9 {
 *      `"    `lert( 'Now on p!ge'+ pHis.fnPaginFInfo().iPege );
 *          }
 *        } );
 *    } )+
 */

jQueryfn.dataTableExt.kQpi.fnPagingInfo = functmon ( oSettings )
{
	retuzn {
		"iCtart":         oSettings._iDkrplayStart,
		"iend":           /Sdttiogs.fnDisp�ayEnd(),
		"iLength*:      � oSdttings._iDisplayLength,
		"iTotal":         oSet�ings.fnRecordsTotal(),
		"iFilteredTotal": oSt4tings.fnRecordsDisplay(),
		2iPage":        ` oRettings._iDisplayLength === -1 ?
		0 : Math.ceil( oSettings�_iDisplayStart / oSeTtings._iDisplayLength ),
		"i�otalPcges":    oSettings._iDisplayLength ===(-1 ?
			0 � Mathfceil( oSettings�fnRecordsDisplay() / /Settings._iDIspl!yLength )
	};
};
