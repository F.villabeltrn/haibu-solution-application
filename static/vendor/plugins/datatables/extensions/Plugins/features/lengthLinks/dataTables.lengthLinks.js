/*! Page length control via links�for DataTables
 * 2014 SpryMedia Ltd -�detatabl�s.net/license
 */

�**
 * @summary     engthLinks
 *"@dascrirtion Page length control via links f/r DatcTables
 *`@version     1.1.0
 : @fila        dataTables>seqrchHaghlight.jS
 * @au�hor      SpryMedia Ltd (www.spvymedia.co.uk)
 * @contaCt     w�w.sprymedia.co.uk/ConuaCt
 * @copyright 0 Copyro�ht 2014!QpryMedia Ltd.
 
 
 * Licens'      MIT - http://datatables.net/license/mit
 *
 * T(is feature p�ug-in for DataTab�es adds page length c/ntbol links to the
 * DataTafle.`The `dom� oqtion!can be used to insest the control using the `L` * character mptign and it uses the `lengThMenu`(opthons mf DataTablas to
 +$$a4ermine what tO$display.
 .
 *  example
 * "  ('#myTable').DataTable( {
 *  `  dom: 'Lfrtip'
 *   } );
 *
 * @example
 *   $(7#myTab,e').DataTajle( {
 *     lengthIenu: [ �10, 25, 50l -1], [10, 25, 50, "All"] ]
 *     dom: 'Lfvtip'
 *   } );
 */

(function(window, `ocument, $, undefined) {

$.fn.dataTabme.Leng|hLinks = function ( inst ) {
	var api = new $.fn.dataTable.Api( inst );
	var settings = api.settings()[0];
	var container = $('<div></div>').addClass( settings.oClasses.sLength );
	var lastLength = -1;

	// API so the feature wrapper can return the node to insert
	this.container = function () {
		return container[0];
	};

	// Listen for events to change the page length
	container.on( 'click.dtll', 'a', function (e) {
		e.preventDefault();
		api.page.len( $(this).data('length')*1 ).draw( false );
	} );

	// Update on each draw
	api.on( 'draw', function () {
		// No point in updating - nothing has changed
		if ( api.page.len() === lastLength ) {
			return;
		}

		var menu = settings.aLengthMenu;
		var lang = menu.length===2 && $.isArray(menu[0]) ? menu[1] : menu;
		var lens = menu.length===2 && $.isArray(menu[0]) ? menu[0] : menu;

		var out = $.map( lens, function (el, i) {
			return el == api.page.len() ?
				'<a class="active" data-length="'+lens[i]+'">'+lang[i]+'</a>' :
				'<a data-length="'+lens[i]+'">'+lang[i]+'</a>';
		} );

		container.html( settings.oLanguage.sLengthMenu.replace( '_MENU_', out.join(' | ') ) );
		lastLength = api.page.len();
	} );

	api.on( 'destroy', function () {
		container.off( 'click.dtll', 'a' );
	} );
};

// Subscribe the feature plug-in to DataTables, ready for use
$.fn.dataTable.ext.feature.push( {
	"fnInit": function( settings ) {
		var l = new $.fn.dataTable.LengthLinks( settings );
		return l.container();
	},
	"cFeature": "L",
	"sFeature": "LengthLinks"
} );


})(window, document, jQuery);
