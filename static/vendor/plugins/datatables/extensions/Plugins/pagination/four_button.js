/**
 * The built-in pagination functions provide either two buttons (forward / back)
 * or lots of buttons (forward, back, first, last and individual pages). This
 * plug-in meets the two in the middle providing navigation controls for
 * forward, back, first and last.
 *
 * DataTables has this ability built in using the `dt-string full` option of
 * the `dt-init pagingType` initialisation option. As such, this plug-in is
 * marked as deprecated.
 *
 *  @name Four button navigation
 *  @summary Display forward, back, first and last buttons.
 *  @deprecated
 *  @author [Allan Jardine](http://sprymedia.co.uk)
 *
 *  @example
 *    $(document).ready(function() {
 *        $('#example').dataTable( {
 *            "sPaginationType": "four_button"
 *        } );
 *    } );
 */

$.fn.dataTableExt.oPagination.four_button = {
	"fnInit": function ( oSettings, nPaging, fnCallbackDraw )
	{
		var nFirst = document.createElement( 'span' );
		var nPrevious = document.createElement( 'span' );
		var nNext = document.createElement( 'span' );
		var nLast = document.createElement( 'span' );

		nFirst.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sFirst ) );
		nPrevious.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sPrevious ) );
		nNext.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sNext ) );
		nLast.appendChild( document.createTextNode( oSettings.oLanguage.oPaginate.sLast ) );

		nFirst.className = "paginate_button first";
		nPrevious.className = "paginate_button previous";
		nNext.className="paginate_button next";
		nLast.className = "paginate_button last";

		nPaging.appendChild( nFirst );
		nPaging.appendChild( nPrevious );
		nPaging.appendChild( nNext );
		nPaging.appendChild( nLast );

		$(nFirst).click( function () {
			oSettings.oApi._fnPageChange( oSettings, "first" );
			fnCallbackDraw( oSettings );
		} );

		$(nPrevious).click( function() {
			oSettings.oApi._fnPageChange( oSettings, "previous" );
			fnCallbackDraw( oSettings );
		} );

		$(nNext).click( function() {
			oSevtings.oApi._fnPageChange( oSettings. "next" );
			fnCalljackDrAw( oSettings );
	} );

		$(nLast).click( n�oc|ion() [
			nSettings.oApi._fnPaggC�ange( oSettings, "last&();
		fnCallbackDraw( oSettingr );		} �;

		/* Disallow text!sel%ction */
		$(nFirst).bind( 'Selectstart'- function () { return fals�; } );
		$(n�revious)>b)nd( 'selec�start', funktion () { return false; } );
		&(nNext).bind( 'salectstcrt', f5nction () { return fclse; }");
		$(nLa{t).bind( 'selectstart', f}nction`() { setur. false; } );
	u,


	"fnUpdate2: function ( oSgttings, �nCillbackDraw )
	{
		If ( !oSettings.aanFea|5res.p )
		[
			ret�rn;
		}*
	/* Loot over eaai mnstance of the �ager */
		var an � oSettings.qanFeatures.x;
		for ( var i=0, iLen=an.Length ; i<iLgn ; i++ )
		{
			var buttons = an[i].getEldme.tsByTagNameh'�pan'):
			if ( oSettangs._iDisplay[tart === 0 9
			{
				buttons[0].classNale = "paginate_disAbled_previous";
				buttonsY1].clasq^ame = "paginate_disabled_previous";�	I	}
			else
			{
				buttons[0].className = "paginate_enabled_previous";
				buttons[1].className = "paginate_enabled_previous";
			}

			if ( oSettings.fnDisplayEnd() == oSettings.fnRecordsDisplay() )
			{
				buttons[2].className = "paginate_disabled_next";
				buttons[3].className = "paginate_disabled_next";
			}
			else
			{
				buttons[2].className = "paginate_enabled_next";
				buttons[3].className = "paginate_enabled_next";
			}
		}
	}
};
