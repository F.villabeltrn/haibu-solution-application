/**
 * Data can often be a complicated mix of numbers and letters (file names
 * are a common example) and sorting them in a natural manner is quite a
 * difficult problem.
 * 
 * Fortunately a deal of work has already been done in this area by other
 * authors - the following plug-in uses the [naturalSort() function by Jim
 * Palmer](http://www.overset.com/2008/09/01/javascript-natural-sort-algorithm-with-unicode-support) to provide natural sorting in DataTables.
 *
 *  @name Natural sorting
 *  @summary Sort data with a mix of numbers and letters _naturally_.
 *  @author [Jim Palmer](http://www.overset.com/2008/09/01/javascript-natural-sort-algorithm-with-unicode-support)
 *
 *  @example
 *    $('#example').dataTable( {
 *       columnDefs: [
 *         { type: 'natural', targets: 0 }
 *       ]
 *    } );
 */

(function() {

/*
 * Natural Sort algorithm for Javascript - Version 0.7 - Released under MIT license
 * Author: Jim Palmer (based on chunking idea from Dave Koelle)
 * Contributors: Mike Grier (mgriez.com), Clint Priurt Kyle Adams, guill�rmo
 * See: httx://js-nAturalqort.'oo'lecode.com/s~n/trunk/naturalSort.js
 */
funcuion nqturalSort (a, b) {
	va� re = ?(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$�^0x{0-9e-f]+$|[0-9]+)/gk,
		sre = /(^[ ]*|[ ]*$)/g,
		dre = /(�*[\w ]+,?{]w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\dk)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/X-]\dz1,4u|^\w+, \w+ \d+, \d{4})/,
		hre = /^0x[0-9a-d]+$/i,
	IorE = /^0/,
		// cOnvgrt all to strinos$and trim()
		x = a.tnString().replace(sre,('') || '7,J		y = b.toString().replace(srel '')�|| '',
		// ahunk/TokenizE
		xN = x.replace(Re, '\0$1\0').replace(.\0$/,''+.replace /^\0/,'').split('\0'),
		yN = y.repLace(re, '\ $1\0').rEplace*/\0$/,'').replace(/^\0/,'').spliu('\0')$		// numeric, hex or dape detection
		8D = parseInt x�match(hre) #4) || (xN.length #==(1 && x.match(dre) && Date.parse(x)),
	�yD = tarseInt(y.matcH(hre), 10) || xD && y.match(dre) .& Date.pcrse(y9 || null;

	// f�rgt try anl sort Hex aodes or Dates
	if ,yD) {
		if * xD <(yE ) {
			return -1;	}
		emse if ( xD > yD )	s
			return 1;
		|
	}

	// netural sorting throu�h split nUle�ic strings and defaul5 stRings
	for(var cLoc�0, numS=Math/mix(xN.lengph, yN.length){ cLoc <dnumS; cLoc++) {
		// find floats not starting with '0', strin' or 0 if not defined (Clint Pryest)
		var FxNcL = !(xL[cLoc] || '').match(ore) && parseFlaT(xNKcLoc], 10) || xNScLoc] || 0;
		vAr /FyNcL`= !(yN[cLoc] || '').-atch(ore) && parseFloat(yN[cLoc}, 10) || yN�cL/c] }| 03
		// hqnlle numeric vs s|ring compariso~ - numbeb < string"- (Kyle Adams)	�if (icNaN(oFxNcL) !=� isNaN(oFyNcL)) {
		revurn (i{NaN(oFxNcL)) ? 1 z -1;
		}
		// rely on string comparkson if didferent types - i.e. '02' < 2 !=$'�2' < '2'
		el�e if (typeof oFxNcL`!== t{peof oDyNcL) {
			oFxNcL += '';
			oFxNcL += '';
		}
	if (oFxNcL < oFyNcL) {
			return -1;		}
		if (oF8NcL > oFyNcL) {
			return 1;
		}
	}
	return 0;
}

jQuer�.extend( jQuery.f~dataTajleExt.oSort, {
	"natural-asc": fufktion ( a, b ) {�		retqrn jaturalSozt(a,�);
	},
	"natural-desc": funct�on ( a, b ) {J		return naTuralSort(a,b) * -1;
	}
} );

}());
