import os
import unittest
from main import app
from functions import aux_calcular, calcular_id, calcular_patente, calcular_matriz,validar_patente, validar_id,validar_numeros

app.testing = True

class test_patente(unittest.TestCase):

    def test_patente_status_code(self):
        with app.test_client() as client:
            response = client.get('/')
            self.assertEqual(response.status_code, 200)

            response = client.post('/',data={'patente':'AAAA000'})
            self.assertEqual(response.status_code, 200)    

    def test_calcular_id_equal(self):
        self.assertEqual(calcular_id("AAAA000"), 1)
        self.assertEqual(calcular_id("AAAB000"), 1001)
        self.assertEqual(calcular_id("ABCD873"), 678874)
        
    def test_calcular_id_not_equal(self):
        self.assertNotEqual(calcular_id("ABCD883"), 678875)
        self.assertNotEqual(calcular_id("ABCD890"), 678878)
    
    def test_calcular_id_raises(self):
        self.assertRaises(ValueError, lambda: calcular_id('AAAAAAA'))
        self.assertRaises(ValueError, lambda: calcular_id('AAAAZZ5'))
    
    def test_validar_patente_true(self):
        self.assertTrue(validar_patente('AAAA000'))
        self.assertTrue(validar_patente('ABZA014'))

    def test_validar_patente_false(self):
        self.assertFalse(validar_patente('A00A015'))
        self.assertFalse(validar_patente('A0*A0A5'))

class test_identificador(unittest.TestCase):

    def test_identificador_status_code(self):
        with app.test_client() as client:
            response = client.get('/identificador')
            self.assertEqual(response.status_code, 200)

            response = client.post('/identificador',data={'identificador':'15'})
            self.assertEqual(response.status_code, 200)   

    def test_calcular_patente_equal(self):
        self.assertEqual(calcular_patente(1001), 'AAAB000')
        self.assertEqual(calcular_patente(1), 'AAAA000')
        self.assertEqual(calcular_patente('678905'), 'ABCD904')
        
    def test_calcular_patente_not_equal(self):
        self.assertNotEqual(calcular_patente(1), 'AAAA020')
        self.assertNotEqual(calcular_patente(50325), 'AZAZ025')
    
    def test_calcular_patente_raises(self):
        self.assertRaises(ValueError, lambda: calcular_patente('AAAAAAA'))
        self.assertRaises(ValueError, lambda: calcular_patente('AA5AZZ5'))
    
    def test_validar_patente_true(self):
        self.assertTrue(validar_id('152'))
        self.assertTrue(validar_id(54687))

    def test_validar_patente_false(self):
        self.assertFalse(validar_patente('A_0AO15'))
        self.assertFalse(validar_patente('A0*A0A5'))
        
class test_matriz(unittest.TestCase):

    def test_matriz_status_code(self):
        with app.test_client() as client:
            response = client.get('/matriz')
            self.assertEqual(response.status_code, 200)

            response = client.post('/matriz',data={'R':'4','C':'3','X':'1','Y':'2','Z':'2'})
            self.assertEqual(response.status_code, 200)   

    def test_calcular_matriz_equal(self):
        #orden de entrada R,C,X,Y,Z

        self.assertEqual(calcular_matriz(4,3,1,2,2), 18)
        self.assertEqual(calcular_matriz(4,3,3,2,2), 36)
        self.assertEqual(calcular_matriz(4,3,3,2,15), 192)
        
    def test_calcular_matriz_not_equal(self):
        self.assertNotEqual(calcular_matriz(4,5,1,3,2), 15)
        self.assertNotEqual(calcular_matriz(4,5,200,3,3), 30)

    def test_calcular_patente_raises(self):
        self.assertRaises(IndexError, lambda: calcular_matriz(4,5,200,5,3))
        self.assertRaises(ValueError, lambda: calcular_matriz(3,'A','-',1,3))
    
    def test_validar_patente_true(self):
        self.assertTrue(validar_numeros('2','3',4,'1',6))
        self.assertTrue(validar_numeros(10,2,3,4,5))

    def test_validar_patente_false(self):
        #orden de entrada R,C,X,Y,Z
        self.assertFalse(validar_numeros('-1',1,1,1,1)) #Validar R > 0
        self.assertFalse(validar_numeros('5','-1',1,1,1)) #Validar C > 0
        self.assertFalse(validar_numeros('5','1','-1',1,1)) #Validar X >= 0
        self.assertFalse(validar_numeros('5','1','1','-1',1)) #Validar Y >= 0
        self.assertFalse(validar_numeros('5','1','1','1','-1')) #Validar Z > 0
        self.assertFalse(validar_numeros('5','1','1','-1',1000001)) #Validar 1000000 > Z
        self.assertFalse(validar_numeros('5','1','1','6',1)) #Validar R > Y
        self.assertFalse(validar_numeros('A','A','A','=','A')) #Validar numeros enteros


if __name__ == '__main__':
    unittest.main()